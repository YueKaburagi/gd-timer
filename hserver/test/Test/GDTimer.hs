{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}

module Test.GDTimer where

import Prelude hiding (runStateT, show)

import Test.Tasty
import Test.Tasty.Hspec
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Test.Hspec

import System.Environment (setEnv)

import Data.Bifunctor (bimap)
import qualified Data.Binary as Binary
import Data.ByteString (ByteString)
import Data.Default.Class (Default(..))
import Data.Function ((&))
import Data.Foldable (find)
import Data.Maybe (fromMaybe, isJust)
import Data.Time.Clock (UTCTime(..), NominalDiffTime, diffUTCTime, addUTCTime)
import Data.Time.Format (FormatTime, TimeLocale, formatTime, defaultTimeLocale)
import Control.Lens (set, over, view, preview, ix, _Just)
import Control.Monad (join)
import Control.Monad.State.Class (get)
import Control.Monad.Trans.State.Lazy (runStateT)
import Control.Monad.Trans.Identity (runIdentityT)
import Text.Show (Show(..))


import GDTimer
import GDTimer.State
import GDTimer.Types
import GDTimer.Types.Util (Transferrable(..))

-- Orphan
instance Show (MVar a) where
  show _ = "MVar"


-- main = do
--   setEnv "TASTY_NUM_THREADS" "3"
--   setEnv "TASTY_QUIET" "true"
--   defaultMain tests
test_tasty :: [TestTree]
test_tasty = [tests]


tests :: TestTree
tests = testGroup "Tests" [
  message
  ]


shouldReturnSatisfy action pred = action >>= (`shouldSatisfy` pred)
shouldSatisfyPredicate action pred = join $ shouldSatisfy <$> action <*> pred


binaries :: TestTree
binaries = testGroup "Binary"
  [ testGroup "Identity"
    [ testProperty "[assert] TfTimerTemporary" $
        \x -> x == (Binary.decode $ Binary.encode (x :: Transferrable TimerTemporary))
    ]
  ]

message :: TestTree
message = testGroup "Invoking Message"
  [ testGroup "add timer"
    [ testCase "[progress] Add Timer (name)" $ do
        perform (invoke (AddTimer "xxx" 30)) `shouldReturnSatisfy`
          (elem "xxx" . fmap (view #timerName) . view #timers . snd)
    , testCase "[progress] Add Timer (time is in valid range)" $ do
        perform (invoke (AddTimer "xxx" 30)) `shouldSatisfyPredicate`
          (predWithUTCTime $ \c ->
              maybe False (\x -> addUTCTime 20 c < x && x < addUTCTime 40 c) .
              preview (_Just . #targetTime) .
              find ((==) "xxx" . view #timerName) .
              view #timers . snd)
    ]
  , testGroup "stop timer"
    [ testGroup "simple timers"
      [ testCase "[progress] Stop Timer" $ do
          perform (invoke (StopTimer "utt")) `shouldReturnSatisfy`
            (notElem "utt" . fmap (view #timerName) . view #timers . snd)
      , testCase "[progress] Register target Timer as ExpiredTimer" $ do
          perform (invoke (StopTimer "utt")) `shouldReturnSatisfy`
            (isJust . preview (#expiredTimers . ix "utt") . snd)
      , testCase "[progress] Cumulate same name ExpiredTimer" $ do
          perform (do
                      invoke (StopTimer "utt")
                      snapshot <- get
                      invoke (AddTimer "utt" 300)
                      invoke (StopTimer "utt")
                      pure snapshot
                  ) `shouldReturnSatisfy`
            (uncurry (<) . bimap
             (fromMaybe 0 . preview (ix "utt" . #spendTime) . view #expiredTimers)
             (fromMaybe 0 . preview (ix "utt" . #spendTime) . view #expiredTimers)
            )
      , testCase "[keep] Don't stop other Timer" $ do
          perform (invoke (StopTimer "utt")) `shouldReturnSatisfy`
            ((==) ["abc","cap"] . fmap (view #timerName) . view #timers . snd)
      , testCase "[error] Stop nonexistence Timer" $ do
          perform (invoke (StopTimer "no such timer")) `shouldReturnSatisfy`
            ((==) (RFailed "no such timer") . fst)
      ]
    , testGroup "chained timers"
      [ testCase "[progress] Stop chained timer" $ do
          perform2 (invoke (StopTimer "chained")) `shouldReturnSatisfy`
            (notElem "chained" . fmap (view #timerName) . view #timers . snd)
      , testCase "[progress] Set next timer automatically" $ do
          perform2 (invoke (StopTimer "chained")) `shouldReturnSatisfy`
            (elem "once" . fmap (view #timerName) . view #timers . snd)
      , testCase "[progress] Set next timer (infinite queue)" $ do
          perform2 (do
                       invoke (StopTimer "every")
                       invoke (StopTimer "every")
                       invoke (StopTimer "every")
                   ) `shouldReturnSatisfy`
            (elem "every" . fmap (view #timerName) . view #timers . snd)
      , testCase "[progress] Come back after a round the ring" $ do
          perform2 (do
                       invoke (StopTimer "pomodoro-on")
                       invoke (StopTimer "pomodoro-off")
                   ) `shouldReturnSatisfy`
            (elem "pomodoro-on" . fmap (view #timerName) . view #timers . snd)
      ]
    ]
  , testGroup "template"
    [ testCase "[progress] Use added template" $ do
        perform (do
                    invoke (RegisterTemplate "template1" False (Listy [templateLike "amia" 30 "dummy.wav"]))
                    invoke (AddTimerFromTemplate "template1")
                ) `shouldReturnSatisfy`
          (elem "amia" . fmap (view #timerName) . view #timers . snd)
    , testCase "[error] Regester empty template" $ do
        perform (invoke (RegisterTemplate "templateEMPTY" False (Listy []))) `shouldReturnSatisfy`
          ((==) (RFailed "cannot register empty template") . fst)
    , testCase "[error] Use undefined template" $ do
        perform (invoke (AddTimerFromTemplate "xxx")) `shouldReturnSatisfy`
          ((==) (RFailed "no such template: xxx") . fst)
    ]
  , testGroup "query timer"
    [ testCase "[progress] Claim exsiting expired timer (name)" $ do
        perform (do
                    invoke (StopTimer "abc")
                    invoke (QuerySpendTime "abc")
                ) `shouldReturnSatisfy`
          ((\(RSpendTime name _) -> name == "abc") . fst)
    , testCase "[progress] Claim existing expired timer (spend time)" $ do
        perform (do
                    invoke (StopTimer "abc")
                    invoke (QuerySpendTime "abc")
                ) `shouldReturnSatisfy`
          ((\(RSpendTime _ secs) -> 0 <= secs && secs < 10) . fst)
    ]
  ]
  where
    perform = underState [timer1 "abc" 4000, timer1 "utt" 300, timer1 "cap" 20]
    perform2 = underState [ timer2 "chained" 20 [timer1 "once" 20]
                          , timer2 "every" 10 (repeat $ timer1 "every" 10)
                          , timer2 "pomodoro-on" 40 (cycle [ timer1 "pomodoro-off" 10
                                                           , timer1 "pomodoro-on" 40
                                                           ])
                          ]


timer1 :: TimerName -> Seconds -> TimerTemplate
timer1 = instantTemplate

timer2 :: TimerName -> Seconds -> [TimerTemplate] -> TimerTemplate
timer2 a b tts = set (_timerTemplate . #nexts) tts $ timer1 a b

underState :: [TimerTemplate] -> TimerIO a -> IO (a, GDTState)
underState ts f = do
  mv <- newEmptyMVar
  dts <- genDummyTimers ts
  runStateT f (set #timers dts $ initGDTState mv)

genDummyTimers :: [TimerTemplate] -> IO [Timer]
genDummyTimers ts = do
  c <- runIdentityT getCurrentTime
  pure $ genTimerFromTemplate0 c <$> ts

predWithUTCTime :: (UTCTime -> a -> Bool) -> IO (a -> Bool)
predWithUTCTime f = do
  c <- runIdentityT getCurrentTime
  pure (f c)
