{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}

module Test.GDTimer.Types where

import Prelude hiding (runStateT, show)

import Test.Tasty
import Test.Tasty.Hspec
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Test.Hspec

import Data.Bifunctor (bimap)
import Data.ByteString (ByteString)
import Data.Default.Class (Default(..))
import Data.Function ((&))
import Data.Foldable (find)
import Data.Maybe (fromMaybe, isJust)
import Data.Time.Clock (UTCTime(..), NominalDiffTime, diffUTCTime, addUTCTime)
import Data.Time.Format (FormatTime, TimeLocale, formatTime, defaultTimeLocale)
import Data.Binary (Binary)
import qualified Data.Binary as Binary
import qualified Data.ByteString.Lazy as BSL
import Control.Lens (set, over, view, preview, ix, _Just)
import Control.Monad (join)
import Control.Monad.State.Class (get)
import Control.Monad.Trans.State.Lazy (runStateT)
import Control.Monad.Trans.Identity (runIdentityT)
import Text.Show (Show(..))


import GDTimer
import GDTimer.State
import GDTimer.Types

firstByteOf :: Binary a => a -> Word8
firstByteOf = BSL.head . Binary.encode
fullBytesOf :: Binary a => a -> [Word8]
fullBytesOf = BSL.unpack . Binary.encode


prop_Upkeep =
  0x01 == firstByteOf Upkeep
prop_StopTimer timerName =
  0x04 == firstByteOf (StopTimer timerName)
prop_AddTimer timerName seconds =
  0x05 == firstByteOf (AddTimer timerName seconds)
prop_AddTimerFromTemplate templateName =
  0x06 == firstByteOf (AddTimerFromTemplate templateName)
prop_RemoveTimer timerName =
  0x07 == firstByteOf (RemoveTimer timerName)
prop_RegisterTemplate templateName isCircular queue =
  0x10 == firstByteOf (RegisterTemplate templateName isCircular queue)
prop_LoadConfig =
  0x11 == firstByteOf LoadConfig
prop_QuerySpendTime timerName =
  0x20 == firstByteOf (QuerySpendTime timerName)
prop_Quit =
  [0xF0,0xDE,0xAD] == fullBytesOf Quit
prop_KillAll =
  0xFD == firstByteOf KillAll
prop_TakeScreenshot saveToPath =
  0xFE == firstByteOf (TakeScreenShot saveToPath)
prop_UnexpectedMessage =
  0xFF == firstByteOf UnexpectedMessage



prop_RSucceed =
  0x01 == firstByteOf RSucceed
prop_RFailed a =
  0xFA == firstByteOf (RFailed a)
prop_RError a =
  0xFE == firstByteOf (RError a)
prop_RTimers a =
  0x10 == firstByteOf (RTimers a)
prop_RSpendTime a b =
  0x20 == firstByteOf (RSpendTime a b)
prop_RUndefined =
  0xFF == firstByteOf RUndefined


prop_BTakeScreenShot a =
  0xC1 == firstByteOf (BTakeScreenShot a)
prop_BUnknown =
  0xFF == firstByteOf BUnknown
prop_BKillAll =
  0xFD == firstByteOf BKillAll

prop_Rhythm a =
  0x01 == firstByteOf (Rhythm a)
prop_Blues a =
  0x80 == firstByteOf (Blues a)
prop_Dissonance =
  0xFF == firstByteOf Dissonance


