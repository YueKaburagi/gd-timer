{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DataKinds, TypeOperators, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedLabels #-}

module GDTimer where

import Prelude hiding (StateT, show)
-- Relude exports several functions form State.Strict

import Data.Extensible
import GHC.TypeLits (symbolVal)

import Data.Default.Class (Default(..))
import Data.Time.Clock (UTCTime(..), diffUTCTime, addUTCTime)
import Data.Time.LocalTime (getZonedTime, zonedTimeToUTC)
import Data.Foldable (foldl)
import qualified Data.List as List
import qualified Data.Map.Strict as Map
import Data.Binary (Binary)
import qualified Data.Binary as Binary
import qualified Data.ByteString as BS
import qualified Data.ByteString.Builder as BS
import qualified Data.ByteString.Lazy as BSL
import Control.Monad.Trans.State.Lazy (StateT)
import Network.Socket as Socket
import qualified Network.Socket.ByteString.Lazy as Socket
import Numeric (showHex)
import Pipes hiding (Proxy)
import Pipes.Core hiding (Proxy)
import Pipes.Network.TCP.Safe -- includes Pipes.Network.TCP
import Pipes.Proxy.Extra
import System.Posix.Files (removeLink)
import Control.Lens (over, view, set, preview, modifying, assign, _Just)
import System.IO.Error.Lens (errorType)
import qualified GHC.IO.Exception as G
import Control.Concurrent (threadDelay)
import Control.Monad.Catch (catch, throwM)
import Data.YAML (FromYAML(..), ToYAML(..), withMap, (.:?), mapping, (.=))
import qualified Data.YAML as YAML
import System.Directory (doesFileExist)

import GDTimer.Types
import GDTimer.Types.Util (domain)
import GDTimer.State


immigrationCounter :: Socket -> MVar Socket -> IO ()
immigrationCounter sock nc = do
  (conn,_connInfo) <- Socket.accept sock -- blocking
  putMVar nc conn -- blocking
  immigrationCounter sock nc


-- orphan
instance Forall (KeyTargetAre KnownSymbol FromYAML) xs => FromYAML (Record xs) where
  parseYAML =
    withMap "Record" $
      \v -> hgenerateFor (Proxy :: Proxy (KeyTargetAre KnownSymbol FromYAML)) $
        \m ->
          let
            k = symbolVal $ proxyKeyOf m
          in
          (v .:? fromString k) >>=
          maybe
            (fail $ "Missing key: " <> k)
            (pure . Field . pure)
instance Forall (KeyTargetAre KnownSymbol ToYAML) xs => ToYAML (Record xs) where
  toYAML =
    mapping . hfoldrWithIndexFor (Proxy :: Proxy (KeyTargetAre KnownSymbol ToYAML)) (
      \m x s -> ((fromString $ symbolVal $ proxyKeyOf m) .= (runIdentity $ getField x)) : s
      ) []


type Config = Record
  '[ "templates" :> [YTemplate]
   ]

type YTemplate = Record
  '[ "templateName" :> TemplateName
   , "isCircular" :> Bool
   , "likes" :> [TemplateLike]
   ]

--utctimeto :: FormatTime t => t -> String
--utctimeto = formatTime defaultTimeLocale "%T"

-- BE LE のために、conn直後にclient側へ deadbeaf :: Word32 でも送ってチェックするべきかも
  
updateTimer :: UTCTime -> Timer -> Timer
updateTimer c t = over #timerTemp ttf t
  where
    tdiff = diffUTCTime (view #targetTime t) c
    ttf = set #diffTime tdiff . set #overTime (tdiff <= 0)


type TimerIO = StateT GDTState IO


configFile :: FilePath
configFile = "config.yaml"
loadConfig :: TimerIO ()
loadConfig = lift (doesFileExist configFile) >>= bool (pure ()) loadConfig0
loadConfig0 :: TimerIO ()
loadConfig0 = do
  bs <- readFileLBS configFile
  case YAML.decode1 bs :: Either (YAML.Pos, String) Config of
    Left x -> lift $ print x
    Right t ->
      traverse apply (view #templates t) >>= lift . print
  where
    apply :: YTemplate -> TimerIO Bool
    apply y = addTemplateFromTemplateLike (view #templateName y) (view #isCircular y) (view #likes y)


readMessage :: BelowPipe' ByteString Message TimerIO r
readMessage = belowM decode
decode :: (MonadFail m, MonadIO m, Binary a) => ByteString -> m a
decode =
  either showAndFail (pure . dropDeco) . Binary.decodeOrFail . toBSL
  where
    -- (Lazy.BS, Int64, String | a)
    showAndFail (src,pos,y) = do
      putStrLn $ foldl (\s f -> f s) "" (showHex <$> BSL.unpack src)
      print pos
      fail y
    dropDeco (_,_,x) = x
    toBSL = BS.toLazyByteString . BS.byteString

responseSection :: BelowPipe' Message ByteString TimerIO r
responseSection = belowM ( fmap (encode . Rhythm) . invoke)
  where
    encode :: Binary a => a -> ByteString
    encode = BSL.toStrict . Binary.encode



timerToExpiredOne :: UTCTime -> Timer -> ExpiredTimer
timerToExpiredOne c t =
  foldr ($) (def :: ExpiredTimer) $
  [ set #timerName (view #timerName t)
  , set #spendTime $ diffUTCTime c $ view #entryTime $ view #timerTemp t
  ]

cumulateExpiredTimer :: ExpiredTimer -> Map TimerName ExpiredTimer -> Map TimerName ExpiredTimer
cumulateExpiredTimer timer = Map.insertWith acc (view #timerName timer) timer
  where
    acc :: ExpiredTimer -> ExpiredTimer -> ExpiredTimer
    acc l r = over #spendTime ((view #spendTime r) +) l

genTimerFromTemplate0 :: UTCTime -> TimerTemplate -> Timer
genTimerFromTemplate0 current shelled =
  foldr ($) (def :: Timer) $
  [ set #timerName (view #timerName template)
  , set #targetTime targetTime
  , over #timerTemp (set #entryTime current)
  , set #timerConf (view #timerConf template)
  , applyNextTemplate
  ]
  where
    template = view _timerTemplate shelled
    dt = fromInteger $ toInteger (view #seconds template :: Seconds)
    targetTime = addUTCTime dt current
    applyNextTemplate =
      case uncons (view #nexts template) of
        Just (x,xs) -> set #nextTemplate (set (_timerTemplate . #nexts) xs <$> Just x)
        Nothing -> id

expireTimer :: [Timer] -> TimerIO ()
expireTimer expires = do
  current <- getCurrentTime
  let etimers = timerToExpiredOne current <$> expires
  modifying #expiredTimers (foldr (\a s -> cumulateExpiredTimer a . s) id etimers)

nextTimer :: [Timer] -> TimerIO ()
nextTimer expires = do
  current <- getCurrentTime
  let nexts = catMaybes $ (preview (#nextTemplate . _Just)) <$> expires
  modifying #timers (fmap (genTimerFromTemplate0 current) nexts <>)

instantTemplate :: TimerName -> Seconds -> TimerTemplate
instantTemplate name secs = genTemplate name secs "sound/pyoro01.wav"
genTemplate :: TimerName -> Seconds -> SoundFile -> TimerTemplate
genTemplate name secs path =
  set _timerTemplate
  ( set #timerName name $
    set #seconds secs $
    set #timerConf (TimerConf path) $
    def) def

genTemplateFromTemplateLike :: TemplateLike -> TimerTemplate
genTemplateFromTemplateLike t =
  genTemplate (view #timerName t) (view #seconds t) (view #soundFile t)

getCurrentTime :: MonadTrans t => t IO UTCTime
getCurrentTime = lift $ zonedTimeToUTC <$> getZonedTime

addTemplateFromTemplateLike :: TemplateName -> Bool -> [TemplateLike] -> TimerIO Bool
addTemplateFromTemplateLike ttName isCircular likes = do
  let templates = genTemplateFromTemplateLike <$> likes
  case uncons (circlet (set (_timerTemplate . #templateName) ttName <$> templates)) of
    Just (x,xs) -> do
      modifying #knownTemplates (Map.insert ttName (set (_timerTemplate . #nexts) xs x))
      pure True
    Nothing -> pure False
  where
    circlet = bool id cycle isCircular


invoke :: Message -> TimerIO Response
invoke = \case
  Upkeep -> do
    timers <- view #timers <$> get
    current <- getCurrentTime
    pure $ RTimers (Listy $ domain . updateTimer current <$> timers)
  StopTimer name -> do
    (inspires, expires) <- List.partition ((/= name) . view #timerName) . view #timers <$> get
    let ret = bool (RFailed "no such timer") RSucceed (not $ null expires)
    assign #timers inspires
    expireTimer expires
    nextTimer expires
    pure ret
  RemoveTimer name -> do
    (inspires, expires) <- List.partition ((/= name) . view #timerName) . view #timers <$> get
    let ret = bool (RFailed "no such timer") RSucceed (not $ null expires)
    assign #timers inspires
    expireTimer expires
    pure ret
  AddTimer name secs -> do
    current <- getCurrentTime
    let timer = genTimerFromTemplate0 current (instantTemplate name secs)
    modifying #timers (timer :)
    pure RSucceed
  AddTimerFromTemplate name -> do
    templates <- view #knownTemplates <$> get
    case Map.lookup name templates of
      Just t -> do
        current <- getCurrentTime
        modifying #timers (genTimerFromTemplate0 current t :)
        pure RSucceed
      Nothing -> pure $ RFailed ("no such template: " <> name)
  RegisterTemplate ttName isCircular queue -> do
    b <- addTemplateFromTemplateLike ttName isCircular (coerce queue)
    if b
      then pure RSucceed
      else pure $ RFailed ("cannot register empty template")
  QuerySpendTime name -> do
    etimers <- view #expiredTimers <$> get
    case Map.lookup name etimers of
      Just t ->
        pure $ RSpendTime (view #timerName t) (floor $ view #spendTime t)
      Nothing -> pure $ RFailed ("no such timer: " <> name)
  KillAll -> do
    modifying #broadcasts (BKillAll :)
    pure RSucceed
  TakeScreenShot name -> do
    modifying #broadcasts (BTakeScreenShot name :)
    pure RSucceed
  Quit -> do
    assign #exit True
    pure RSucceed -- or QuitReserved?
  _ ->
    pure $ RError "unexpected message"


-- Proxy x' x r Socket TimerIO ()
-- BiPipe x' Socket () ByteString TimerIO r
-- />/
mainloop :: TimerIO ()
mainloop = do
  -- check new connection
  nSock <- lift . tryTakeMVar =<< view #newConnection <$> get
  case nSock of
    Just conn -> modifying #connections (conn :)
    Nothing -> pure ()
  -- broadcast
  brs <- view #broadcasts <$> get
  if not (null brs) then broadcast brs else pure ()
  assign #broadcasts mempty
  -- poll all socks
  socks <- view #connections <$> get
  traverse_ (runEffect . oneSection) socks
  lift $ threadDelay 100000
  end <- view #exit <$> get
  bool mainloop (pure ()) end
  where
    sendAll :: Socket -> BSL.ByteString -> TimerIO ()
    sendAll sock bsl =
      catch
      (lift $ Socket.sendAll sock bsl)
      (\e ->
         case view errorType e of
           G.ResourceVanished -> removeSocket sock
           _ -> throwM e
      )
    broadcast :: [Broadcast] -> TimerIO ()
    broadcast brs = do
      let evts = Binary.encode . Blues <$> brs
      socks <- view #connections <$> get
      traverse_ (`sendAll` BSL.concat evts) socks
    source :: Socket -> Producer' ByteString TimerIO ()
    source sock =
      catch
      (fromSocketTimeout 10000 sock 4096)
      (\e ->
          case view errorType e of
            G.TimeExpired -> pure ()
            G.ResourceVanished -> lift $ removeSocket sock
            _ -> throwM e
        )
    destination :: Socket -> Consumer' ByteString TimerIO ()
    destination sock =
      catch
      (toSocket sock)
      (\e ->
         case view errorType e of
           G.ResourceVanished -> lift $ removeSocket sock
           _ -> throwM e
      )
    oneSection :: Socket -> Effect' TimerIO ()
    oneSection sock =
      (source sock >>~ readMessage >>~ responseSection) >-> destination sock

removeSocket :: Socket -> TimerIO ()
removeSocket sock = do
  lift $ Socket.close sock
  modifying #connections $ filter (/= sock)



unlink :: SockAddr -> IO ()
unlink (SockAddrUnix addr) = removeLink addr
unlink _ = pure ()

genRecvSock :: AddrInfo -> IO Socket
genRecvSock info = do
  unlink (addrAddress info)
  sock <- socket (addrFamily info) (addrSocketType info) (addrProtocol info)
  bind sock (addrAddress info)
  Socket.listen sock 2
  pure sock






