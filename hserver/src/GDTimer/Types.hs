{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DataKinds, TypeOperators, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedLabels #-}


module GDTimer.Types ( module GDTimer.Types
                     , module GDTimer.Types.Common ) where

import Data.Extensible

import Data.Default.Class (Default(..))
import Data.Binary (Binary)
import qualified Data.Binary as Binary
import Data.Binary.Put (putWord8, putWord16be)
import Data.Binary.Get (Get, getWord8, getWord16be)
import Control.Lens (set, view)
import Test.QuickCheck (Arbitrary(..), Arbitrary1(..), arbitrary1, shrink1)
import qualified Test.QuickCheck as Arbitrary (oneof)

import GDTimer.Types.Util
import GDTimer.Types.Common


type Path = TransferableString

newtype Listy a = Listy [a] deriving (Show, Eq, Ord, Generic)
instance Binary a => Binary (Listy a) where
  put (Listy xs) = do
    putWord16be (w16L xs)
    traverse_ Binary.put xs
  get = do
    len <- getWord16be
    Listy <$> replicateM (fromEnum len) Binary.get
instance Arbitrary1 Listy where
  liftArbitrary x = Listy . take 0xFFFF <$> liftArbitrary x
instance Arbitrary a => Arbitrary (Listy a) where
  arbitrary = arbitrary1
  shrink = shrink1


type TemplateLike = Record
 '[ "timerName" :> TimerName
  , "seconds" :> Seconds
  , "soundFile" :> Path
  ]
templateLike :: TimerName -> Seconds -> Path -> TemplateLike
templateLike name secs path =
  set #timerName name $
  set #seconds secs $
  set #soundFile path $
  def

data Message = UnexpectedMessage
             | Upkeep
             | StopTimer !TimerName
             | AddTimer !TimerName !Seconds
             | AddTimerFromTemplate !TemplateName
             | RemoveTimer !TimerName
             | RegisterTemplate !TemplateName !Bool !(Listy TemplateLike)
             | LoadConfig
             | QuerySpendTime !TimerName
             | KillAll
             | TakeScreenShot !Path
             | Quit
             deriving (Show, Eq, Ord, Generic)
data Response = RUndefined
              | RSucceed
              | RFailed TransferableString -- ^ 正常失敗
              | RTimers (Listy (Transferrable Timer))
              | RSpendTime TimerName Seconds
              | RError TransferableString
              deriving (Show, Eq, Ord)
data Broadcast = BUnknown
               | BTakeScreenShot Path
               | BKillAll
               deriving (Show, Eq, Ord)
data RandB = Rhythm Response | Blues Broadcast | Dissonance
           deriving (Show, Eq, Ord)

instance Arbitrary Message where
  arbitrary = Arbitrary.oneof
              [ pure Upkeep
              , StopTimer <$> arbitrary
              , RemoveTimer <$> arbitrary
              , AddTimer <$> arbitrary <*> arbitrary
              , AddTimerFromTemplate <$> arbitrary
              , RegisterTemplate <$> arbitrary <*> arbitrary <*> arbitrary
              , pure LoadConfig
              , QuerySpendTime <$> arbitrary
              , TakeScreenShot <$> arbitrary
              , pure KillAll
              , pure Quit
              , pure UnexpectedMessage
              ]
instance Arbitrary Response where
  arbitrary = Arbitrary.oneof
              [ pure RSucceed
              , RFailed <$> arbitrary
              , RError <$> arbitrary
              , RTimers <$> liftArbitrary arbitraryTimer
              , RSpendTime <$> arbitrary <*> arbitrary
              , pure RUndefined
              ]
instance Arbitrary Broadcast where
  arbitrary = Arbitrary.oneof
              [ BTakeScreenShot <$> arbitrary
              , pure BKillAll
              , pure BUnknown
              ]

instance Default TemplateLike where
  def = #timerName @= ""
        <: #seconds @= 0
        <: #soundFile @= ""
        <: emptyRecord

instance Binary TemplateLike where
  put t = do
    Binary.put (view #timerName t)
    Binary.put (view #seconds t)
    Binary.put (view #soundFile t)
  get = do
    tN <- Binary.get :: Get TimerName
    secs <- Binary.get :: Get Seconds
    path <- Binary.get :: Get Path
    pure $ foldr ($) (def :: TemplateLike) $
      [set #timerName tN, set #seconds secs, set #soundFile path]
instance Binary Message where
  put = \case
    Upkeep -> putWord8 0x01
    StopTimer name -> putWord8 0x04 >> Binary.put name
    AddTimer name secs -> do
      putWord8 0x05
      Binary.put name
      Binary.put secs
    AddTimerFromTemplate name -> putWord8 0x06 >> Binary.put name
    RemoveTimer name -> putWord8 0x07 >> Binary.put name
    RegisterTemplate name isCircular queue ->
      putWord8 0x10 >> Binary.put name >> Binary.put isCircular >> Binary.put queue
    LoadConfig -> putWord8 0x11
    QuerySpendTime name -> putWord8 0x20 >> Binary.put name
    Quit -> putWord8 0xF0 >> putWord16be 0xDEAD
    KillAll -> putWord8 0xFD
    TakeScreenShot name -> putWord8 0xFE >> Binary.put name
    UnexpectedMessage -> putWord8 0xFF
  get = do
    header <- getWord8
    case header of
      0x01 -> pure Upkeep
      0x04 -> StopTimer <$> Binary.get
      0x05 -> AddTimer <$> Binary.get <*> Binary.get
      0x06 -> AddTimerFromTemplate <$> Binary.get
      0x07 -> RemoveTimer <$> Binary.get
      0x10 -> RegisterTemplate <$> Binary.get <*> Binary.get <*> Binary.get
      0x11 -> pure LoadConfig
      0x20 -> QuerySpendTime <$> Binary.get
      0xF0 -> do
        v <- getWord16be -- これ gW16に失敗したらどうなるの？
        pure $ bool UnexpectedMessage Quit (v == 0xDEAD)
      0xFD -> pure KillAll
      0xFE -> TakeScreenShot <$> Binary.get
      _ -> pure UnexpectedMessage
instance Binary Response where
  put = \case
    RSucceed -> putWord8 0x01
    RFailed str -> putWord8 0xFA >> Binary.put str
    RError err -> putWord8 0xFE >> Binary.put err
    RTimers timers -> putWord8 0x10 >> Binary.put timers
    RSpendTime name secs -> putWord8 0x20 >> Binary.put name >> Binary.put secs
    RUndefined -> putWord8 0xFF
  get = do
    header <- getWord8
    case header of
      0x01 -> pure RSucceed
      0xFA -> RFailed <$> Binary.get
      0xFE -> RError <$> Binary.get
      0x10 -> pure RUndefined
      0x20 -> RSpendTime <$> Binary.get <*> Binary.get
      _ -> pure RUndefined
instance Binary Broadcast where
  put = \case
    BTakeScreenShot str -> putWord8 0xC1 >> Binary.put str
    BKillAll -> putWord8 0xFD
    BUnknown -> putWord8 0xFF
  get = do
    header <- getWord8
    case header of
      0xC1 -> BTakeScreenShot <$> Binary.get
      0xFD -> pure BKillAll
      _ -> pure BUnknown
instance Binary RandB where
  put = \case
    Rhythm r -> putWord8 0x01 >> Binary.put r
    Blues b -> putWord8 0x80 >> Binary.put b
    Dissonance -> putWord8 0xFF
  get = do
    header <- getWord8
    case header of
      0x01 -> Rhythm <$> Binary.get
      0x80 -> Blues <$> Binary.get
      _ -> pure Dissonance

