{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies, FlexibleContexts #-}

module GDTimer.Types.Util where

import Data.Time.Calendar (Day(..))
import Data.Time.Clock (UTCTime(..), NominalDiffTime)
import Data.Binary (Binary)
import qualified Data.Binary as Binary
import qualified Data.ByteString as BS
import qualified Data.ByteString.Builder as BS
import qualified Data.ByteString.Lazy as BSL


-- 残り時間
difftimeToStr :: NominalDiffTime -> String
difftimeToStr t = format (floor t)
  where
    format :: Int -> String
    format u = show (u `div` 86400) <> "." <> hh (u `mod` 86400)
    hh u = fix2 (u `div` 3600) <> ":" <> mm (u `mod` 3600)
    mm u = fix2 (u `div` 60) <> ":" <> fix2 (u `mod` 60)
    -- Formatter.format (left 2 '0') num
    -- Formatter.left 2 '0' % Formatter.int num
    fix2 s = reverse $ take 2 (reverse (show s) <> repeat '0')
-- 過去の処理は別でしないといけないかも？
-- さっき -1.23.59.59 から 0 に近づく動きになったので48時間で帰ってくる
--  同様に 2日以上先の予定とかも上手くいくか怪しい
-- { utctDay :: UTCTime -> Day }
-- diffDays :: Day -> Day -> Integer
-- addDays :: Integer -> Day -> Day
type DiffDay = Integer



w16L :: [a] -> Word16
w16L = toEnum . length
w16LBS :: ByteString -> Word16
w16LBS = toEnum . BS.length
capsule :: ByteString -> ByteString
capsule bs = BSL.toStrict (Binary.encode $ w16LBS bs) <> bs
-- word16 /big endian
fixString :: String -> ByteString
fixString = BSL.toStrict . BS.toLazyByteString . BS.stringUtf8
nullTime :: UTCTime
nullTime = UTCTime (ModifiedJulianDay 0) 0


type family Transferrable a :: *
class Binary (Transferrable a) => Domain a where
  domain :: a -> Transferrable a
