{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds, TypeOperators, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedLabels #-}

module GDTimer.Types.Common where

import Data.Extensible

import Data.Default.Class (Default(..))
import Data.Time.Clock (UTCTime(..), NominalDiffTime)
import Data.Foldable (foldl)
import Data.Binary (Binary)
import qualified Data.Binary as Binary
import Data.Binary.Put (putWord16be, putByteString)
import qualified Data.Binary.Put as Binary
import Data.Binary.Get (getWord16be, getByteString, Get)
import qualified Data.Binary.Get as Binary
import qualified Data.ByteString.Builder as BS
import qualified Data.ByteString.Lazy as BSL
import Control.Lens (set, view, iso, Iso')
import Test.QuickCheck (Arbitrary(..), Gen(..))
import Data.YAML (FromYAML(..), ToYAML(..))
import qualified Data.Text.Lazy.Encoding as Text
import qualified Data.Text.Lazy.Builder as Text
import qualified Data.Text.Lazy as Text

import GDTimer.Types.Util



newtype Seconds = Seconds Word64
                deriving (Show, Eq, Ord, Num, Integral, Real, Enum)
instance Binary Seconds where
  put (Seconds secs) = Binary.putWord64be secs
  get = Seconds <$> Binary.getWord64be
instance Arbitrary Seconds where
  arbitrary = Seconds <$> arbitrary
instance FromYAML Seconds where
  parseYAML = fmap Seconds . parseYAML
instance ToYAML Seconds where
  toYAML (Seconds secs) = toYAML secs

newtype TransferableString = TrnStr ByteString
                           deriving (Show, Eq, Ord, Generic)
instance Binary TransferableString where
  put (TrnStr bs) = do
    putWord16be (w16LBS bs)
    putByteString bs
  get = do
    l <- getWord16be
    TrnStr <$> getByteString (fromEnum l)
instance IsString TransferableString where
  fromString = TrnStr . fixString
instance Semigroup TransferableString where
  a <> b = TrnStr (coerce a <> coerce b)
instance Monoid TransferableString where
  mempty = ""
instance Arbitrary TransferableString where
  arbitrary = fromString <$> arbitrary
instance FromYAML TransferableString where
  parseYAML = fmap (TrnStr . BSL.toStrict . Text.encodeUtf8 . Text.toLazyText . Text.fromText) . parseYAML
instance ToYAML TransferableString where
  toYAML (TrnStr bs) = toYAML (Text.toStrict $ Text.decodeUtf8 $ BS.toLazyByteString $ BS.byteString bs)


-- RequestId <^> ResponseId ?
type TimerName = TransferableString
type TemplateName = TransferableString
type SoundFile = TransferableString
-- SoundFile + SoundVolume(-dB)
type EntryTime = UTCTime
type TargetTime = UTCTime
type SpendTime = NominalDiffTime
data TimerConf = TimerConf SoundFile deriving (Show, Eq, Ord, Generic)

instance Arbitrary TimerConf where
  arbitrary = TimerConf <$> arbitrary

type ExpiredTimer = Record
  '[ "timerName" :> TimerName
   , "spendTime" :> SpendTime
   ]
type TimerTemporary = Record
  '[ "diffTime" :> NominalDiffTime
   , "overTime" :> Bool
   , "entryTime" :> EntryTime
   ]
type TfTimerTemporary = Record
  '[ "seconds" :> Seconds
   , "overTime" :> Bool
   ]
type instance Transferrable TimerTemporary = TfTimerTemporary
-- | Evade cycle in type synonym.
type InternalTimerTemplate = Record
  '[ "templateName" :> TemplateName
   , "timerName" :> TimerName
   , "seconds" :> Seconds
   , "timerConf" :> TimerConf
   , "nexts" :> [TimerTemplate]
   ]
data TimerTemplate = TimerTemplate InternalTimerTemplate deriving (Show, Eq, Ord, Generic)
_timerTemplate :: Iso' TimerTemplate InternalTimerTemplate
_timerTemplate = iso (\(TimerTemplate t) -> t) TimerTemplate
type TfTimer = Record
  '[ "timerName" :> TimerName
   , "timerTemp" :> TfTimerTemporary -- Transferrable TimerTemporary
   , "timerConf" :> TimerConf
   ]
type Timer = Record
  '[ "timerName" :> TimerName
   , "targetTime" :> TargetTime
   , "timerTemp" :> TimerTemporary
   , "timerConf" :> TimerConf
   , "nextTemplate" :> Maybe TimerTemplate
   ]
type instance Transferrable Timer = TfTimer


instance Default ExpiredTimer where
  def = #timerName @= ""
        <: #spendTime @= 0
        <: emptyRecord
instance Default TimerConf where
  def = TimerConf ""
instance Default TfTimerTemporary where
  def = #seconds @= 0
        <: #overTime @= False
        <: emptyRecord
instance Default TimerTemporary where
  def = #diffTime @= 0
        <: #overTime @= False
        <: #entryTime @= nullTime
        <: emptyRecord
instance Default InternalTimerTemplate where
  def = #templateName @= ""
        <: #timerName @= ""
        <: #seconds @= 0
        <: #timerConf @= def
        <: #nexts @= mempty
        <: emptyRecord
instance Default TimerTemplate where
  def = TimerTemplate def
instance Default TfTimer where
  def = #timerName @= ""
        <: #timerTemp @= def
        <: #timerConf @= def
        <: emptyRecord
instance Default Timer where
  def = #timerName @= ""
        <: #targetTime @= nullTime
        <: #timerTemp @= def
        <: #timerConf @= def
        <: #nextTemplate @= Nothing
        <: emptyRecord

instance Binary TimerConf where
  put (TimerConf soundfile) = Binary.put soundfile
  get = TimerConf <$> Binary.get
type instance Transferrable TimerConf = TimerConf
instance Domain TimerConf where domain = id

instance Binary TfTimerTemporary where
  put tt = do
    Binary.put (view #seconds tt)
    Binary.put (view #overTime tt)
  get = do
    secs <- Binary.get :: Get Seconds
    ot <- Binary.get :: Get Bool
    pure $ foldl (&) (def :: TfTimerTemporary) $
      [set #seconds secs, set #overTime ot]
instance Domain TimerTemporary where
  domain tt = #seconds @= (floor $ view #diffTime tt)
              <: #overTime @= (view #overTime tt)
              <: emptyRecord

instance Binary TfTimer where
  put t = do
    Binary.put (view #timerName t)
    Binary.put (view #timerTemp t)
    Binary.put (view #timerConf t)
  get = do
    tN <- Binary.get :: Get TimerName
    tT <- Binary.get :: Get (Transferrable TimerTemporary)
    tC <- Binary.get :: Get (Transferrable TimerConf)
    pure $ foldl (&) (def :: TfTimer) $
      [set #timerName tN, set #timerTemp tT, set #timerConf tC]
instance Domain Timer where
  domain t = #timerName @= (view #timerName t)
             <: #timerTemp @= (domain $ view #timerTemp t)
             <: #timerConf @= (view #timerConf t)
             <: emptyRecord

arbitraryTimer :: Gen (Transferrable Timer)
arbitraryTimer = do
    name <- arbitrary
    pure $ set #timerName name def



