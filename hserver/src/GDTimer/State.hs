{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DataKinds, TypeOperators, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedLabels #-}

module GDTimer.State where

import Data.Extensible
import Network.Socket as Socket

import GDTimer.Types

type GDTState = Record
  '[ "newConnection" :> MVar Socket
   , "connections" :> [Socket]
   , "exit" :> Bool
   , "broadcasts" :> [Broadcast]
   , "timers" :> [Timer]
   , "expiredTimers" :> Map TimerName ExpiredTimer
   , "knownTemplates" :> Map TemplateName TimerTemplate
   ]
initGDTState :: MVar Socket -> GDTState
initGDTState mvSock = #newConnection @= mvSock
                      <: #connections @= mempty
                      <: #exit @= False
                      <: #broadcasts @= mempty
                      <: #timers @= mempty
                      <: #expiredTimers @= mempty
                      <: #knownTemplates @= mempty
                      <: emptyRecord

