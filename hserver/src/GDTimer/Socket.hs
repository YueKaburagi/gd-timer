
module GDTimer.Socket where

import Network.Socket


genUnixAddr :: String -> AddrInfo
genUnixAddr addr =
  defaultHints { addrFamily = AF_UNIX
               , addrSocketType = Stream
               , addrAddress = SockAddrUnix addr
               }

genIP4Addr :: HostAddress -> PortNumber -> AddrInfo
genIP4Addr addr port =
  defaultHints { addrFamily = AF_INET
               , addrSocketType = Stream
               , addrAddress = SockAddrInet port addr
               }

