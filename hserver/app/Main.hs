{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLabels #-}

module Main (main) where

import Prelude hiding (StateT, runStateT, Proxy)
-- Relude exports several functions form State.Strict

import Data.Default.Class (Default(..))
import Data.Time.Calendar (Day(..))
import Data.Time.Clock (UTCTime(..))
import qualified Data.List as List
import qualified Data.Map.Strict as Map
import Data.Binary (Binary)
import qualified Data.Binary as Binary
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Builder as BS
import qualified Data.ByteString.Lazy as BSL
import Control.Monad.Trans.State.Lazy (StateT, runStateT)
import Network.Socket as Socket
import qualified Network.Socket.ByteString.Lazy as Socket
import Numeric (showHex)
import Pipes
import Pipes.Core
import Pipes.Network.TCP.Safe -- includes Pipes.Network.TCP
import Pipes.Proxy.Extra
import System.Posix.Files (removeLink)
import Control.Lens (makeLensesFor, over, view, set, preview, modifying, assign)
import System.IO.Error.Lens (errorType)
import qualified GHC.IO.Exception as G
import Control.Concurrent (forkIO, killThread, threadDelay)
import Control.Monad.Catch (catch, throwM)

import GDTimer
import GDTimer.State
import GDTimer.Socket

main :: IO ()
main = do
  sock <- genRecvSock (genIP4Addr addr port)
  mv <- newEmptyMVar
  _ <- runStateT (inter sock) (initGDTState mv)
  close sock
  where
    addr = tupleToHostAddress (0x7F,0,0,0x01)
    port = 33333
    inter sock = do
      mv <- view #newConnection <$> get
      receiver <- lift $ forkIO $ immigrationCounter sock mv
      loadConfig
      mainloop
      lift $ killThread receiver

