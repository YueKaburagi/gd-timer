extends Object

var dict: Dictionary = {} # [name -> TimerInfo]
var destroy_queue: Array = []

var screenshot_target_file: String = ""
var fatal: bool = false
var suicide: bool = false

var dump_mode: bool = false
var test_mode: bool = false




func reserve_abort_program():
	fatal = true


func reserve_take_screenshot(path: String):
	screenshot_target_file = path


func done_screenshot():
	screenshot_target_file = ""


func reserve_suicide():
	suicide = true
