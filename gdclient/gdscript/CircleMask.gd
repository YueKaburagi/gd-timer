extends Node2D


var time = 0.0
var time_l = 16.0
var center = Vector2(0,0)
var radius = 140
var color = Color(1, 1, 1, 1.0)


func _ready():
	pass


func _process(_delta):
	time += _delta
	if time > time_l:
		time -= time_l
	update()


func _draw():
	var part = time / time_l
	draw_pie(center, radius, 360 * part, 360, color)


func draw_pie(center, radius, angle_beg, angle_end, color):
	var nb_points = 8
	var points_arc = PoolVector2Array()

	points_arc.push_back(center)

	for i in range(nb_points + 1):
		var angle_point = deg2rad(angle_beg + i * (angle_end - angle_beg) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	for index_point in range(nb_points):
		draw_colored_polygon(points_arc, color)
