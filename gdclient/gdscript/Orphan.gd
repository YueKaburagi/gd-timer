extends Node


var dict: Dictionary = {} # [name -> TimerInfo]
var timer_pane = preload("res://TimerPane.tscn")
var tick: float = 0.0

var TimerInfo = load("res://gdscript/TimerInfo.gd")
var BinaryIO = load("res://gdscript/BinaryIO.gd")
var State = load("res://gdscript/State.gd")

var binaryIO = null
var state = null


func _init():
	Logger.set_logger_level(Logger.LOG_LEVEL_ALL)
	OS.low_processor_usage_mode = true
	state = State.new()
	for arg in OS.get_cmdline_args():
		match arg:
			"--dump":
				state.dump_mode = true
			"--test":
				state.test_mode = true
			_:
				# do nothing
				pass


func _ready():
	binaryIO = BinaryIO.new()
	binaryIO.initialize(state)
	if state.fatal:
		set_process(false)
	$Infomation.set_text_from_status(binaryIO.lastStatus)
	tick = 1.0 # reserve initial update



func _process(_delta):
	binaryIO.update_state()
	$Infomation.set_text_from_status(binaryIO.lastStatus)
	check_new_timers()
	destroy_queued_timers()
	check_reserves()
	if state.dict.empty():
		$Infomation.show()
	else:
		$Infomation.hide()
	tick += _delta
	if tick >= 1.0:
		tick = max(0.0, tick - floor(tick))
		binaryIO.send_upkeep()
		$Infomation.set_text_from_status(binaryIO.lastStatus)


func _exit_tree():
	# expire dictonary
	for t in dict.values():
		expire_timer(t)
	dict.clear()
	# take screenshot if in test mode
	if state.test_mode:
		take_screenshot("test99-00")
	pass


func take_screenshot(name: String):
	var img = get_viewport().get_texture().get_data()
	img.flip_y()
	var path = "res://temporary/ss/%s.png" % name
	img.save_png(path)
	Logger.debug("take_screenshot(); Screenshot saved at '%s'" % path)


func check_new_timers():
	for t in state.dict.values():
		if t.instance == null:
			add_new_panel(t)
		else:
			pass


func destroy_queued_timers():
	for t in state.destroy_queue:
		expire_timer(t)
	state.destroy_queue = []


func check_reserves():
	match state.screenshot_target_file:
		"":
			pass
		var _s:
			take_screenshot(_s)
	if state.suicide:
		set_process(false)
	state.done_screenshot()



func send_stop_or_remove(timer_name: String, isRemove: bool):
	binaryIO.send_stop_or_remove(timer_name, isRemove)
	$Infomation.set_text_from_status(binaryIO.lastStatus)
	tick += 1.0 # reserve force update


func add_new_panel(info): # info: TimerInfo
	var o = timer_pane.instance()
	o.get_node("StopOrNextButton").connect("pressed", self, "send_stop_or_remove", [info.name, false])
	o.get_node("RemoveButton").connect("pressed", self, "send_stop_or_remove", [info.name, true])
	o.timer = info
	info.instance = o
	get_node("MainView/TimerContainer").add_child(o)


func expire_timer(timer): # TimerInfo
	if timer.instance != null:
		get_node("MainView/TimerContainer").remove_child(timer.instance)
	timer.expire()


