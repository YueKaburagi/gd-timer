extends Object

var peer = null
var lastStatus = null

var TimerInfo = load("res://gdscript/TimerInfo.gd")
var State = load("res://gdscript/State.gd")

var state = null # State



func _init():
	pass


func initialize(main_state):
	state = main_state
	peer = StreamPeerTCP.new()
	peer.big_endian = true
	gen_connection()
	var status = peer.get_status()
	if status == StreamPeerTCP.STATUS_ERROR:
		peer.disconnect_from_host()
		OS.execute("./hserver", ["+RTS"], false)
		OS.delay_usec(10000)
		gen_connection()
	status = peer.get_status()
	lastStatus = status
	if state.fatal:
		return
	if status == StreamPeerTCP.STATUS_ERROR:
		Logger.error("_init(); Fail to connect. CODE: unknown")
		state.reserve_abort_program()
	lastStatus = status


func gen_connection():
	var err = peer.connect_to_host("127.0.0.1", 33333)
	if err != OK:
		Logger.error("gen_connection(); Fail to connect. CODE: %d" % err)
		state.reserve_abort_program()

func update_state():
	match prepare_receive():
		false:
			pass
		var _l:
			while 0 < peer.get_available_bytes():
				if !state.dump_mode:
					read_data()
				else:
					dump_data(_l)
					#peer.get_partial_data(4094) # > /dev/null


func prepare_receive(): # -> false or int
	var status = peer.get_status()
	if status == StreamPeerTCP.STATUS_CONNECTED:
		lastStatus = status
		var length = peer.get_available_bytes()
		if length > 0:
			return length
		else:
			return false
	elif lastStatus != status:
		Logger.warn("prepare_receive(); Peer status changed. '%d' to '%d'" % [lastStatus,status])
		lastStatus = status
	return false


func dump_data(length):
	if length > 0:
		var rets = peer.get_partial_data(length)
		match check_valid_data(rets):
			false:
				pass
			var _data:
				Logger.debug("dump_data(); Dump rest data.\n%s" % _data.hex_encode())


func read_data():
	var head = peer.get_u8()
	match head:
		0x01:
			read_response()
		0x80:
			read_broadcast()
		_:
			#unexpected
			Logger.error("read_data(); unexpected index: '%02X'" % head)
			pass


func read_response():
	var head = peer.get_u8()
	match head:
		0x01:
			#succeed
			# do nothing
			pass
		0xFA:
			#failure
			var _failure = read_string()
			Logger.warn("read_response(); Receive FAILURE: %s" % _failure)
			pass
		0xFE:
			#error
			var _err = read_string()
			Logger.error("read_response(); Receive ERROR: %s" % _err)
			pass
		0x10:
			#timers
			read_timers()
		_:
			#undefined
			Logger.error("read_response(); unexpected index: '%02X'" % head)
			pass
	pass


func read_broadcast():
	var head = peer.get_u8()
	match head:
		0xC1:
			#take screen shot
			var _filename = read_string()
			state.reserve_take_screenshot(_filename)
			pass
		0xFD:
			#kill all
			state.reserve_suicide()
		_:
			#unknown
			Logger.error("read_broadcast(); unexpected index: '%02X'" % head)
			pass
	pass


func read_timers():
	var n = peer.get_u16()
	var l = state.dict.keys()
	for _i in range(n):
		l.erase(read_timer())
	for x in l:
		var timer = state.dict[x]
		state.destroy_queue.push_back(timer)
		var _r = state.dict.erase(x)




func read_timer():
	var name = read_string()
	var timer = state.dict.get(name, TimerInfo.new())
	timer.name = name
	timer.set_time( peer.get_u64() )
	timer.overtimed = false if peer.get_u8() == 0 else true
	timer.sound_file_path = "user://%s" % read_timer_conf()
	state.dict[name] = timer
	if timer.instance != null:
		timer.instance.update_info()
	return name


func read_string() -> String:
	var l = peer.get_u16()
	var p = peer.get_data(l)
	match check_valid_data(p):
		false:
			return ""
		var _data:
			return _data.get_string_from_utf8()


func check_valid_data(data_array): # -> false or String
	match data_array:
		[var _err, var _data]:
			if _err != OK:
				Logger.error("check_valid_data(); Fail to receive data. CODE: %d" % _err)
				return false
			else:
				return _data
		_:
			Logger.error("check_valid_data(); Unexpected %s" % data_array)
			return false
		

func read_timer_conf() -> String:
	return read_string()










func gen_transferrable_string(string: String) -> PoolByteArray:
	var binary = string.to_utf8()
	var data = to_u16be(binary.size())
	data.append_array(binary)
	return data


func to_u16be(n: int) -> PoolByteArray:
	var low = 0xFF & n
	var high = 0xFF & (n >> 8)
	return PoolByteArray([high, low])


func send_stop_or_remove(name: String, isRemove: bool = false):
	if prepare_send():
		var binary = null
		if isRemove:
			binary = PoolByteArray([0x07])
		else:
			binary = PoolByteArray([0x04])
		binary.append_array(gen_transferrable_string(name))
		aftercare_send(peer.put_data(binary))


func send_upkeep():
	if prepare_send():
		peer.put_u8(0x01)


func prepare_send() -> bool:
	var status = peer.get_status()
	if status != StreamPeerTCP.STATUS_CONNECTED:
		Logger.error("prepare_send(); Status code: %d" % status)
		lastStatus = status
		return false
	else:
		return true


func aftercare_send(err):
	if err != OK:
		Logger.error("aftercare_send(); Fail to send data. CODE: %d" % err)

