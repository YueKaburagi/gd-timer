extends HBoxContainer

# A Timer Panel
class_name TimerPane

var TimerInfo = load("res://gdscript/TimerInfo.gd")

var timer = null # TimerInfo


# expire():?
# anim; height -> 0 ?
# suspend $Player ?

func _ready():
#	$Player.stream = AudioStreamSample.new()
#	$Player.stream.loop_mode = AudioStreamSample.LOOP_FORWARD
	pass

func _process(_delta):
	update_info()


func update_info():
	if timer != null:
		var cx = get_children()
		for c in cx:
			match c.name:
				"TimerName":
					c.text = timer.name
					if timer.overtimed:
						c.set_theme(preload("res://emphasised.tres"))
				"TimerTime":
					c.text = timer.time
				"StopOrNextButton":
					c.text = timer.button
				"Player":
					if c.stream == null or c.stream.resource_path != timer.sound_file_path:
						c.stream = load(timer.sound_file_path)
						if c.stream == null:
							Logger.error("update_info(); Fail to load %s" % timer.sound_file_path.replace("user:/", OS.get_user_data_dir()))
#						c.stream.loop_mode = AudioStreamSample.LOOP_FORWARD
						c.volume_db = -3.0
				_:
					pass
		if timer.overtimed:
			_play_sound()


func _play_sound():
	if not $Player.playing:
		$Player.play()
