extends Object

class_name TimerInfo

var name: String = "<name>"
var time: String = "<12:34:56>"
var instance = null # Panel
var default_message: String = "stop"
var button: String = "<push>"
var overtimed: bool = false
var sound_file_path: String = "hoge"


func expire():
	if instance != null:
		instance.timer = null
	instance = null


func _ready():
	pass



func set_time(n: int):
	var z = false
	if n < 0:
		z = true
		n = -n
	var s = n % 60
	var m = (n / 60) % 60
	var h = (n / 3600) % 24
	var d = n / (24 * 3600)
	if z:
		time = "-%d.%02d:%02d:%02d" % [d,h,m,s]
	else:
		time = "%d.%02d:%02d:%02d" % [d,h,m,s]

