extends RichTextLabel


var last = StreamPeerTCP.STATUS_NONE

func _ready():
	pass

func set_text_from_status(status):
	last = status
	match status:
		StreamPeerTCP.STATUS_CONNECTED:
			bbcode_text = "[center]No scheduled timers.[/center]"
		StreamPeerTCP.STATUS_CONNECTING:
			bbcode_text = "[center]Connecting to server.[/center]"
			self.show()
		StreamPeerTCP.STATUS_NONE:
			bbcode_text = "[center]Missing connection.[/center]"
			self.show()
		_:
			bbcode_text = "[center]Error[/center]"
			self.show()


func hide():
	if last == StreamPeerTCP.STATUS_CONNECTED:
		.hide()
