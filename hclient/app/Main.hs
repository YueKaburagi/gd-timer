{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Relude

import System.Environment (getArgs)
import Text.Parsec (parse, try)
import Text.Parsec.Char
import Text.Parsec.Combinator
import qualified Data.ByteString.Builder as BS
import qualified Data.ByteString.Lazy as BSL
import Network.Socket as Socket
import qualified Data.Binary as Binary
import qualified Network.Socket.ByteString as S
import Text.Read (read)
import System.Process (spawnProcess)
import Control.Monad.Catch (catch, throwM)
import qualified GHC.IO.Exception as G
import System.IO.Error (ioeGetErrorType)
import Control.Concurrent (threadDelay)
import Options.Applicative

import qualified GDTimer as GDT
import GDTimer.Types
import GDTimer.Socket
import Client

main :: IO ()
main =
  sendTimerServer =<< execParser (info (opus <**> helper) fullDesc)

opus =
  subparser
  (  command "set" (info parseSet $ progDesc "Set timer.")
  <> command "template" (
      info (
          subparser
          (  command "add" (info parseTemplateAdd $ progDesc "Register timer template.")
          <> command "use" (info parseTemplateUse $ progDesc "Use registered template.")
          ) <**> helper
          ) $ progDesc "See template --help.")
  <> command "spend" (
      info (
          subparser
          ( command "time" (info parseSpendTime $ progDesc "Print spend time.")
          ) <**> helper
          ) $ progDesc "See spend --help.")
  <> command "screenshot" (info parseScreenshot $ progDesc "Instruct GUI client to take screenshot.")
  <> command "killall" (info parseKillall $ progDesc "Send KillAll message.")
  <> command "quit" (info parseQuit $ progDesc "Send Quit message.")
  )

parseSet = AddTimer
  <$> strArgument (metavar "<timer-namr>")
  <*> argument (eitherReader timeToSeconds) (metavar "<time>")
parseTemplateAdd = RegisterTemplate
  <$> strArgument (metavar "<template-name>")
  <*> pure False
  <*> fmap Listy (some parseTemplate)
parseTemplateUse = AddTimerFromTemplate
  <$> strArgument (metavar "<template-name>")
parseSpendTime = QuerySpendTime
  <$> strArgument (metavar "<timer-name>")
parseScreenshot = TakeScreenShot
  <$> strArgument (metavar "<file-name>")
parseKillall = pure KillAll
parseQuit = pure Quit


timeToInt :: (Applicative m, MonadFail m) => String -> m Int
timeToInt pref =
  case parse time "arg" pref of
    Left _ -> fail "invalid time formant"
    Right (Time h m s) -> pure $ h*3600 + m*60 + s
timeToSeconds :: String -> Either String Seconds
timeToSeconds = fmap toEnum . timeToInt

parseTemplate = templateLike
  <$> strArgument (metavar "<timer-name>")
  <*> argument (eitherReader timeToSeconds) (metavar "<time>")
  <*> pure "sound/pyoro01.wav"


activateServer :: IO ()
activateServer =
  void $ spawnProcess "./hserver" ["+RTS"]

genConnSock :: AddrInfo -> IO Socket
genConnSock info = do
  sock <- prepSock
  catch (gen sock)
    (\e ->
      case ioeGetErrorType e of
        G.NoSuchThing -> do
          Socket.close' sock -- 失敗したsocketを捨てる
          activateServer
          threadDelay 10000 -- server起動をちょっと待つ
          nsock <- prepSock -- socketから作り直す
          gen nsock -- retry once
        _ -> throwM e
    )
  where
    prepSock = Socket.socket (addrFamily info) (addrSocketType info) (addrProtocol info)
    gen sock = do
      Socket.connect sock (addrAddress info)
      pure sock


sendTimerServer :: Message -> IO ()
sendTimerServer msg = do
  sock <-genConnSock (genIP4Addr addr port)
  S.sendAll sock (encode msg)
  r <- S.recv sock 4096
  Socket.close sock
  print =<< decode r
  where
    addr = tupleToHostAddress (0x7F,0,0,0x01)
    port = 33333


encode :: Message -> ByteString
encode = BSL.toStrict . Binary.encode

decode :: ByteString -> IO RandB
decode = GDT.decode

