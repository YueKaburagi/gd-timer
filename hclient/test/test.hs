
import Test.Tasty
import Test.Tasty.Hspec
import Test.Tasty.HUnit
import Test.Hspec

import Text.Parsec (parse)

import Client


main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [parsing]


parsing :: TestTree
parsing =
  testGroup "Parse"
  [ testCase "hms" $ do
      parse time "test" "12h34m56s" `shouldBe` Right (Time 12 34 56)
  , testCase "s" $ do
      parse seconds "test" "s" `shouldBe` Right "s"
  ]
