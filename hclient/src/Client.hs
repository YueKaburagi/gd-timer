{-# LANGUAGE NoImplicitPrelude #-}
{-# LaNGUAGE FlexibleContexts #-}

module Client where

import Relude

import Text.Parsec (try, Stream, ParsecT)
import Text.Parsec.Char
import Text.Parsec.Combinator
import Text.Read (read)

data Time = Time Int Int Int
          deriving (Show, Eq, Ord)

-- set <> 1:00:00 ; ??
-- set <> at 12oclock

-- bad 123sm123mh234u

time :: ParsecT String u Identity Time         
time = do
  h <- fromMaybe 0 <$> optionMaybe (try (num <* hours))
  m <- fromMaybe 0 <$> optionMaybe (try (num <* minutes))
  s <- fromMaybe 0 <$> optionMaybe (try (num <* seconds))
  pure $ Time h m s
num = read <$> some digit
seconds =
  try (string "secs") <|> try (string "sec") <|> string "s"
minutes =
  try (string "mins") <|> try (string "min") <|> string "m"
hours =
  try (string "hours") <|> try (string "hour") <|> string "h"
