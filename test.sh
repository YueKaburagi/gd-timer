#!/bin/sh


(
    cd hserver
    stack test
)

(
    cd hclient
    stack test
)


(
    cd hserver
    stack install --local-bin-path ../gdclient
    cp -v config.yaml ../gdclient
    cd ../hclient
    cd ../gdclient
    rm temporary/ss/*
    godot &
    sleep 5
    cd ../hclient
    stack run -- screenshot test00-00
    sleep 1
)
(
    cd hclient
    stack run -- set test 50m
    sleep 1
    stack run -- screenshot test01-00
    sleep 1
)
(
    cd hclient
    stack run -- set test2 1h2m3s
    sleep 1
    stack run -- screenshot test01-01
    sleep 1
)
(
    cd hclient
    stack run -- template use sasa
    sleep 1
    stack run -- screenshot test01-02
    sleep 1
)


(
    cd gdclient
    rm .reg/expected/*
    mv .reg/actual/* .reg/expected/
    reg-suit run
    cd ../hclient
    stack run -- killall
    sleep 1
    stack run -- quit
)
