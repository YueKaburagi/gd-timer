#!/bin/sh

mkdir -p bin

(
    cd hserver
    stack install --local-bin-path ../bin
)

(
    cd hclient
    stack install --local-bin-path ../bin
)

(
    cd gdclient
    godot-headless --export "Linux/X11" ../bin/gdclient.x86_64
)

cp -uv LICENSE bin/
cp -uv MATERIALS.md bin/
cp -uvr MATERIALS.LICENSES bin/
